#pragma once

#include <core/Renderer.h>

PXR_NAMESPACE_OPEN_SCOPE

// Forward declarations.
class HdMirageRenderBuffer;


class HdMirageRenderer final
{
public:
    /// Renderer constructor.
    HdMirageRenderer();

    /// Renderer destructor.
    ~HdMirageRenderer();

private:
    // The width of the render buffers.
    unsigned int _width;
    // The height of the render buffers.
    unsigned int _height;

    Mirage::Scene _scene;
};

PXR_NAMESPACE_CLOSE_SCOPE