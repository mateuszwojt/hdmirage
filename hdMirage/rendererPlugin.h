#pragma once

#include <pxr/imaging/hd/rendererPlugin.h>

PXR_NAMESPACE_OPEN_SCOPE

/// Plugin entry points (construction, destruction) for the tri render delegate.
class HdMirageRendererPlugin final : public HdRendererPlugin
{
public:
    HdMirageRendererPlugin() = default;
    ~HdMirageRendererPlugin() override = default;

    /// Cannot copy.
    HdMirageRendererPlugin(const HdMirageRendererPlugin&) = delete;
    HdMirageRendererPlugin& operator=(const HdMirageRendererPlugin&) = delete;

    /// Create a new Mirage render delegate instance.
    virtual HdRenderDelegate* CreateRenderDelegate() override;

    /// Delete a Mirage render delegate instance.
    virtual void DeleteRenderDelegate(
        HdRenderDelegate* renderDelegate) override;

    /// Is this plugin supported?
    virtual bool IsSupported() const override;
};

PXR_NAMESPACE_CLOSE_SCOPE