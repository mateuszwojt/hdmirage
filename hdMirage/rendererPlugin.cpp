#include "renderDelegate.h"
#include "rendererPlugin.h"

#include <pxr/imaging/hd/rendererPluginRegistry.h>

PXR_NAMESPACE_OPEN_SCOPE

TF_REGISTRY_FUNCTION(TfType)
{
    HdRendererPluginRegistry::Define<HdMirageRendererPlugin>();
}

HdRenderDelegate*
HdMirageRendererPlugin::CreateRenderDelegate()
{
    return new HdTriRenderDelegate();
}

void
HdMirageRendererPlugin::DeleteRenderDelegate(HdRenderDelegate* renderDelegate)
{
    delete renderDelegate;
}

bool
HdMirageRendererPlugin::IsSupported() const
{
    return true;
}

PXR_NAMESPACE_CLOSE_SCOPE